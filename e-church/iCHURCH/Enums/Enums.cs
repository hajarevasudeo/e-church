﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iCHURCH
{
    public enum Gender
    {
        Male,
        Female,
    }

    public enum RegistrationType
    {
        Birth,
        Death,
        Marriage,
    }
}
