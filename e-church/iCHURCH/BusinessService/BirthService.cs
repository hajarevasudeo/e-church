﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iCHURCH.DataAccess;

namespace iCHURCH
{
    public class BirthService : iCHURCH.BusinessService.IBirthService 
    {
        private IBirthRecordAccess memberservice;

        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public BirthService()
        {
            this.memberservice = new BirthRecordAccess();
        }

        /// <summary>
        /// deletes record
        /// </summary>
        public bool DeleteBirthRecord(int id)
        {
            return this.memberservice.DeleteBirthRecord(id);
        }

        /// <summary>
        /// Get All records
        /// </summary>
        public System.Data.DataTable GetAllBirthRecords()
        {
            return this.memberservice.GetAllBirthRecords();
        }

        /// <summary>
        /// Get Single Record
        /// </summary>
        public System.Data.DataRow GetBirthRecordById(int id)
        {
            return this.memberservice.GetBirthRecordById(id);
        }

        /// <summary>
        /// add new
        /// </summary>
        public bool RegisterBirthRecord(BirthRecordModel birthRecord)
        {
            return this.memberservice.AddBirthRecord(birthRecord);
        }

        /// <summary>
        /// search
        /// </summary>
        public System.Data.DataTable SearchBirthRecord(string name)
        {
            return this.memberservice.SearchBirthRecord(name);
        }

        /// <summary>
        /// update existing
        /// </summary>
        public bool UpdateBirthRecord(iCHURCH.BirthRecordModel birthRecord)
        {
            throw new System.NotImplementedException();
        }
    }
}
