﻿using System;
using System.Data;

namespace iCHURCH.BusinessService
{
    public interface IBirthService
    {
     
        bool DeleteBirthRecord(int id);
        System.Data.DataTable GetAllBirthRecords();
        System.Data.DataRow GetBirthRecordById(int id);
        bool RegisterBirthRecord(iCHURCH.BirthRecordModel birthRecord);
        System.Data.DataTable SearchBirthRecord(string name);
        bool UpdateBirthRecord(iCHURCH.BirthRecordModel birthRecord);
    }
}
