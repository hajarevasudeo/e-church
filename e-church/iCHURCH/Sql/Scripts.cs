﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iCHURCH
{
    public static class Scripts
    {
        /// <summary>
        /// Sql to get a club member details by Id
        /// </summary>
        public static readonly string sqlGetBirthRecordById = "Select" +
            " Id, Name, DateOfBirth, Occupation, MaritalStatus, HealthStatus, Salary, NumberOfChildren" +
            " From ClubMember Where Id = @Id";

        /// <summary>
        /// Sql to get all club members
        /// </summary>
        public static readonly string SqlGetAllBirthRecords = "Select" +
            " Id, Name, DateOfBirth, Occupation, MaritalStatus, HealthStatus, Salary, NumberOfChildren" +
            " From ClubMember";

        /// <summary>
        /// sql to insert a club member details
        /// </summary>
        public static readonly string SqlInsertBirthRecord = "Insert Into" +
            " ClubMember(Name, DateOfBirth, Occupation, MaritalStatus, HealthStatus, Salary, NumberOfChildren)" +
            " Values(@Name, @DateOfBirth, @Occupation, @MaritalStatus, @HealthStatus, @Salary, @NumberOfChildren)";

        /// <summary>
        /// sql to search for club members
        /// </summary>
        public static readonly string SqlSearchBirthRecord = "Select " +
            " Id, Name, DateOfBirth, Occupation, MaritalStatus, HealthStatus, Salary, NumberOfChildren" +
            " From ClubMember Where (@Occupation Is NULL OR @Occupation = Occupation) {0}" +
            " (@MaritalStatus Is NULL OR @MaritalStatus = MaritalStatus)";

        /// <summary>
        /// sql to update club member details
        /// </summary>
        public static readonly string SqlUpdateBirthRecord = "Update ClubMember " +
            " Set [Name] = @Name, [DateOfBirth] = @DateOfBirth, [Occupation] = @Occupation, [MaritalStatus] = @MaritalStatus, " +
            " [HealthStatus] = @HealthStatus, [Salary] = @Salary, [NumberOfChildren] = @NumberOfChildren Where ([Id] = @Id)";

        /// <summary>
        /// sql to delete a club member record
        /// </summary>
        public static readonly string sqlDeleteBirthRecord = "Delete From ClubMember Where (Id = @Id)";
    }
}
