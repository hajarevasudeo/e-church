﻿

namespace iCHURCH
{
    using System.Configuration;

    /// <summary>
    /// ConnectionAccess class
    /// </summary>
    public abstract class ConnectionAccess
    {
        /// <summary>
        /// Gets connection string
        /// </summary>
        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager
                    .ConnectionStrings["SocialClubDBConnection"]
                    .ToString();
            }
        }
    }
}
