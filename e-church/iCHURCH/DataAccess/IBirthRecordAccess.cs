﻿using System;
namespace iCHURCH.DataAccess
{
    public interface IBirthRecordAccess
    {
        bool AddBirthRecord(iCHURCH.BirthRecordModel birthRecord);
        bool DeleteBirthRecord(int id);
        System.Data.DataTable GetAllBirthRecords();
        System.Data.DataRow GetBirthRecordById(int id);
        System.Data.DataTable SearchBirthRecord(string operand);
        bool UpdateBirthRecord(iCHURCH.BirthRecordModel birthRecord);
    }
}
