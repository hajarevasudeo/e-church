﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iCHURCH
{
    public partial class BirthRecord
    {
        /*
        public static Boolean  createBirtRecord(String FullName,  DateTime RegistarionDate,String ChurchName,String Gender,String BirthPlace,    DateTime BirthDate,String FathersName, String MothersName,String Address,String PaternalGrandFather,String PaternalGrandMother,String MaternalGrandFather,String MaternalGrandMother,String GodFather,String GodMother,String BaptismMinister,String RegisterBookYear,String RegisterBookNumber,String RegisterBookChurch,   DateTime RegisterBookDate,String Remarks)
        {

            try
            {

                BirthRecord br = new BirthRecord();
                br.FullName = FullName;
                br.RegistarionDate = RegistarionDate;
                br.ChurchName = ChurchName;
                br.Gender = Gender;
                br.BirthPlace = BirthPlace;
                br.BirthDate = BirthDate;
                br.FathersName = FathersName;
                br.MothersName = MothersName;
                br.Address = Address;
                br.PaternalGrandFather = PaternalGrandFather;
                br.PaternalGrandMother = PaternalGrandMother;
                br.MaternalGrandFather = MaternalGrandFather;
                br.MaternalGrandMother = MaternalGrandMother;
                br.GodFather = GodFather;
                br.GodMother = GodMother;
                br.BaptismMinister = BaptismMinister;
                br.RegisterBookYear = RegisterBookYear;
                br.RegisterBookNumber = RegisterBookNumber;
                br.RegisterBookChurch = RegisterBookChurch;
                br.RegisterBookDate = RegisterBookDate;
                br.InsertTimeStamp = DateTime.UtcNow;
                br.Remarks = Remarks;

                ECHURCHDBSQLEntities entity = new ECHURCHDBSQLEntities();

                entity.AddToBirthRecords(br);
                entity.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }
          */
        public static Boolean createBirtRecord(BirthRecord objBirthRecord)
        {
            try
            {
                ECHURCHDBSQLEntities entity = new ECHURCHDBSQLEntities();
                if (objBirthRecord != null)
                {
                    objBirthRecord.InsertTimeStamp = DateTime.Now;
                    entity.AddToBirthRecords(objBirthRecord);
                    entity.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static Boolean updateBirtRecord(Int32 Id ,String FullName, DateTime RegistarionDate, String ChurchName, String Gender, String BirthPlace, DateTime BirthDate, String FathersName, String MothersName, String Address, String PaternalGrandFather, String PaternalGrandMother, String MaternalGrandFather, String MaternalGrandMother, String GodFather, String GodMother, String BaptismMinister, String RegisterBookYear, String RegisterBookNumber, String RegisterBookChurch, DateTime RegisterBookDate)
        {

            return true;

        }

        public static Boolean deleteBirtRecord(Int32 Id)
        {
            return true;
        }
    }
}
