﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml.simpleparser;
using iTextSharp.text.html.simpleparser;
using System.IO;

namespace eCHURCH.Desktop.Source
{
    class Common
    {

        public static void GenerateCertificate(String fileName, iCHURCH.RegistrationType type)
        {
           
            FileStream fs = new FileStream(System.Environment.CurrentDirectory + "\\" + fileName, FileMode.Create);//it needs to be changed
            Document document = new Document(PageSize.A4, 25, 25, 30, 30);
            HTMLWorker htmlWorker = new HTMLWorker(document);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();
            string htmlData = GetHtmlTemplate(type);
            StringReader strBuilderData = new StringReader(htmlData);
            htmlWorker.Parse(strBuilderData);
            document.Close();
            writer.Close();
            fs.Close();
            System.Diagnostics.Process.Start(fileName);

        }

        public static String GetHtmlTemplate(iCHURCH.RegistrationType type)
        {
            String htmlTemplate = String.Empty;
            switch (type)
            {
                case iCHURCH.RegistrationType.Birth:
                    {
                        htmlTemplate  = File.ReadAllText(Resource.DataLocalisation.TemplatePath.BaptismTemplatePath_Test_2);
                        //htmlTemplate = htmlTemplate.Replace("[image]", "<img width='100px' height='100px' src='" + System.Environment.CurrentDirectory + "\\Images\\logo-rvbc1.gif'/>");
                        htmlTemplate = htmlTemplate.Replace("<image></image>", "<img width='100px' height='100px' src='" + System.Environment.CurrentDirectory + "\\Images\\cert.jpg'/>");
                        break;
                    }
                case iCHURCH.RegistrationType.Death:
                    {
                        htmlTemplate  = File.ReadAllText(Resource.DataLocalisation.TemplatePath.DeathTemplatePath);
                        break;
                    }
                case iCHURCH.RegistrationType.Marriage:
                    {
                        htmlTemplate  = File.ReadAllText(Resource.DataLocalisation.TemplatePath.DeathTemplatePath);
                        break;
                    }
                default:
                    {
                        break;
                    }
             }
           
          
            return htmlTemplate;
        }
    
    }
}

