﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTextSharp;

using System.IO;
using System.Web;
using eCHURCH.Desktop.Source;
using iCHURCH;

namespace eCHURCH.Desktop.UI
{
    public partial class frmRegistration : Form
    {
        public frmRegistration()
        {
            InitializeComponent();
        }

        private Boolean validateBaptismData()
        {
            if (String.IsNullOrEmpty(txtFullName.Text.Trim()))
            {
                MessageBox.Show(eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_req_validation_full_name,
                                       eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_record_warning,
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
                return false;
            }
            else if (!(rdoMale.Checked) && !(rdoFemale.Checked))
            {
                
              MessageBox.Show(eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_req_validation_gender,
                                       eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_record_warning,
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
              return false;
            }
            else if(dtpBirthDate.Value.Date >=  DateTime.Now.Date)
            {
                MessageBox.Show(eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_validation_dateOfBirth,
                                      eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_validation_dateOfBirth,
                                      MessageBoxButtons.OK,
                                      MessageBoxIcon.Information);
            }

            return true;
        }
       private BirthRecord mapBaptismData()
       {
           BirthRecord objBirthRecord = new BirthRecord();
           objBirthRecord.FullName = txtFullName.Text;
           objBirthRecord.Address = txtAddress.Text;
           objBirthRecord.BirthDate = dtpBirthDate.Value.Date;
           objBirthRecord.BirthPlace = txtBirthPlace.Text;
           objBirthRecord.ChurchName = "";
           objBirthRecord.FathersName = txtFathersName.Text;
           objBirthRecord.Gender = rdoMale.Checked ? Gender.Male.ToString() : Gender.Female.ToString();
           objBirthRecord.GodFather = txtGodFather.Text;
           objBirthRecord.GodMother = txtGodMother.Text;
           objBirthRecord.MaternalGrandFather = txtMGrandFather.Text;
           objBirthRecord.MaternalGrandMother = txtMGrandMother.Text;
           objBirthRecord.MothersName = txtMotherName.Text;
           objBirthRecord.PaternalGrandFather = txtPGrandFather.Text;
           objBirthRecord.PaternalGrandMother = txtPGrandMother.Text;
           objBirthRecord.RegistarionDate = DateTime.Now;
           objBirthRecord.RegisterBookChurch = "";
           objBirthRecord.RegisterBookDate = DateTime.Now;
           objBirthRecord.RegisterBookNumber = "";
           objBirthRecord.RegisterBookYear = "";
           objBirthRecord.Remarks = txtRemarks.Text;
           return objBirthRecord;
       }
        private void btnRegister_Click(object sender, EventArgs e)
        {
            Boolean result=false;
            if (validateBaptismData())
            {
                BirthRecord objBirthRecord = mapBaptismData();
                if (objBirthRecord != null)
                {
                    result = BirthRecord.createBirtRecord(objBirthRecord);
                }

                if (result)
                {
                    var res = MessageBox.Show(eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_record_created,
                                             eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_record_success,
                                             MessageBoxButtons.OK,
                                             MessageBoxIcon.Information);
                }
                else
                {

                    var res = MessageBox.Show(eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_record_not_created,
                                             eCHURCH.Desktop.Resource.DataLocalisation.Messages.baptism_record_failure,
                                             MessageBoxButtons.OK,
                                             MessageBoxIcon.Error);
                }
            }
          
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
             txtFullName.Text="";
             txtAddress.Text="";
           // dtpBirthDate.Value.Date = DateTime.MinValue.Date;
             txtBirthPlace.Text="";
             txtFathersName.Text="";
             rdoMale.Checked = false;
             rdoFemale.Checked = false;
             txtGodFather.Text="";
             txtGodMother.Text="";
             txtMGrandFather.Text="";
             txtMGrandMother.Text="";
             txtMotherName.Text="";
             txtPGrandFather.Text="";
             txtPGrandMother.Text="";
             txtRemarks.Text="";
        }

    }
}
