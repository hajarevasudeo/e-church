﻿namespace eCHURCH.Desktop.UI
{
    partial class frmRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabRegistration = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabBirth = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabRemarrks = new System.Windows.Forms.TabControl();
            this.tabRemarks = new System.Windows.Forms.TabPage();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.tabAddressDetails = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.tabFamilyDetails = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMGrandMother = new System.Windows.Forms.TextBox();
            this.txtPGrandMother = new System.Windows.Forms.TextBox();
            this.txtMGrandFather = new System.Windows.Forms.TextBox();
            this.txtPGrandFather = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMotherName = new System.Windows.Forms.TextBox();
            this.txtFathersName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPersonalDetails = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.rdoFemale = new System.Windows.Forms.RadioButton();
            this.rdoMale = new System.Windows.Forms.RadioButton();
            this.dtpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvSearchResult_Birth = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.dtpSearchDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.txtSearchFirstName = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSearchLastName = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.txtWProfession = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtWBaptizedChurch = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtWResitingAt = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtBirthPlace = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtWMothersName = new System.Windows.Forms.TextBox();
            this.txtWFathersName = new System.Windows.Forms.TextBox();
            this.txtWLastName = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtWFirstName = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl6 = new System.Windows.Forms.TabControl();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.txtHProfession = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtHBaptizedChurch = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtHResidingAt = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtHBirthPlace = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtHMothersName = new System.Windows.Forms.TextBox();
            this.txtHFathersName = new System.Windows.Forms.TextBox();
            this.txtHLastName = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtHFirstName = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabControl7 = new System.Windows.Forms.TabControl();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.txtGodFather = new System.Windows.Forms.TextBox();
            this.txtGodMother = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.tabRegistration.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabBirth.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabRemarrks.SuspendLayout();
            this.tabRemarks.SuspendLayout();
            this.tabAddressDetails.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabFamilyDetails.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPersonalDetails.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearchResult_Birth)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabControl6.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl7.SuspendLayout();
            this.tabPage16.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabRegistration
            // 
            this.tabRegistration.Controls.Add(this.tabPage1);
            this.tabRegistration.Controls.Add(this.tabPage2);
            this.tabRegistration.Controls.Add(this.tabPage3);
            this.tabRegistration.ItemSize = new System.Drawing.Size(92, 25);
            this.tabRegistration.Location = new System.Drawing.Point(21, 22);
            this.tabRegistration.Name = "tabRegistration";
            this.tabRegistration.SelectedIndex = 0;
            this.tabRegistration.Size = new System.Drawing.Size(908, 701);
            this.tabRegistration.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage1.Controls.Add(this.btnCancel);
            this.tabPage1.Controls.Add(this.tabBirth);
            this.tabPage1.Controls.Add(this.btnRegister);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(900, 668);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Birth ";
            // 
            // tabBirth
            // 
            this.tabBirth.Controls.Add(this.tabPage5);
            this.tabBirth.Controls.Add(this.tabPage4);
            this.tabBirth.Location = new System.Drawing.Point(20, 23);
            this.tabBirth.Name = "tabBirth";
            this.tabBirth.SelectedIndex = 0;
            this.tabBirth.Size = new System.Drawing.Size(827, 581);
            this.tabBirth.TabIndex = 32;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tabControl4);
            this.tabPage5.Controls.Add(this.tabRemarrks);
            this.tabPage5.Controls.Add(this.tabAddressDetails);
            this.tabPage5.Controls.Add(this.tabFamilyDetails);
            this.tabPage5.Controls.Add(this.tabPersonalDetails);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(819, 555);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Registration";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(751, 621);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabRemarrks
            // 
            this.tabRemarrks.Controls.Add(this.tabRemarks);
            this.tabRemarrks.Location = new System.Drawing.Point(13, 453);
            this.tabRemarrks.Name = "tabRemarrks";
            this.tabRemarrks.SelectedIndex = 0;
            this.tabRemarrks.Size = new System.Drawing.Size(781, 88);
            this.tabRemarrks.TabIndex = 3;
            // 
            // tabRemarks
            // 
            this.tabRemarks.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabRemarks.Controls.Add(this.txtRemarks);
            this.tabRemarks.Location = new System.Drawing.Point(4, 22);
            this.tabRemarks.Name = "tabRemarks";
            this.tabRemarks.Size = new System.Drawing.Size(773, 62);
            this.tabRemarks.TabIndex = 0;
            this.tabRemarks.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(24, 18);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(705, 26);
            this.txtRemarks.TabIndex = 49;
            // 
            // tabAddressDetails
            // 
            this.tabAddressDetails.Controls.Add(this.tabPage8);
            this.tabAddressDetails.Location = new System.Drawing.Point(434, 15);
            this.tabAddressDetails.Name = "tabAddressDetails";
            this.tabAddressDetails.SelectedIndex = 0;
            this.tabAddressDetails.Size = new System.Drawing.Size(368, 150);
            this.tabAddressDetails.TabIndex = 2;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage8.Controls.Add(this.txtAddress);
            this.tabPage8.Controls.Add(this.textBox19);
            this.tabPage8.Controls.Add(this.label25);
            this.tabPage8.Controls.Add(this.label26);
            this.tabPage8.Controls.Add(this.textBox22);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(360, 124);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "Address Details";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(16, 18);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(324, 88);
            this.txtAddress.TabIndex = 48;
            // 
            // textBox19
            // 
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox19.Location = new System.Drawing.Point(78, -43);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(160, 20);
            this.textBox19.TabIndex = 47;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(275, -36);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 13);
            this.label25.TabIndex = 41;
            this.label25.Text = "Address Line 2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(2, -36);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "Address Line 1";
            // 
            // textBox22
            // 
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox22.Location = new System.Drawing.Point(373, -38);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(160, 20);
            this.textBox22.TabIndex = 46;
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(649, 621);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(75, 23);
            this.btnRegister.TabIndex = 3;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // tabFamilyDetails
            // 
            this.tabFamilyDetails.Controls.Add(this.tabPage6);
            this.tabFamilyDetails.Location = new System.Drawing.Point(15, 181);
            this.tabFamilyDetails.Name = "tabFamilyDetails";
            this.tabFamilyDetails.SelectedIndex = 0;
            this.tabFamilyDetails.Size = new System.Drawing.Size(783, 172);
            this.tabFamilyDetails.TabIndex = 1;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage6.Controls.Add(this.label9);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.label7);
            this.tabPage6.Controls.Add(this.label6);
            this.tabPage6.Controls.Add(this.txtMGrandMother);
            this.tabPage6.Controls.Add(this.txtPGrandMother);
            this.tabPage6.Controls.Add(this.txtMGrandFather);
            this.tabPage6.Controls.Add(this.txtPGrandFather);
            this.tabPage6.Controls.Add(this.label5);
            this.tabPage6.Controls.Add(this.label4);
            this.tabPage6.Controls.Add(this.label3);
            this.tabPage6.Controls.Add(this.label1);
            this.tabPage6.Controls.Add(this.txtMotherName);
            this.tabPage6.Controls.Add(this.txtFathersName);
            this.tabPage6.Controls.Add(this.label16);
            this.tabPage6.Controls.Add(this.label17);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(775, 146);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Family Details";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(74, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "(Maternal)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(461, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "(Maternal)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(461, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "(Paternal)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(74, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "(Paternal)";
            // 
            // txtMGrandMother
            // 
            this.txtMGrandMother.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMGrandMother.Location = new System.Drawing.Point(521, 100);
            this.txtMGrandMother.Name = "txtMGrandMother";
            this.txtMGrandMother.Size = new System.Drawing.Size(220, 20);
            this.txtMGrandMother.TabIndex = 28;
            // 
            // txtPGrandMother
            // 
            this.txtPGrandMother.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPGrandMother.Location = new System.Drawing.Point(521, 57);
            this.txtPGrandMother.Name = "txtPGrandMother";
            this.txtPGrandMother.Size = new System.Drawing.Size(220, 20);
            this.txtPGrandMother.TabIndex = 27;
            // 
            // txtMGrandFather
            // 
            this.txtMGrandFather.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMGrandFather.Location = new System.Drawing.Point(144, 102);
            this.txtMGrandFather.Name = "txtMGrandFather";
            this.txtMGrandFather.Size = new System.Drawing.Size(220, 20);
            this.txtMGrandFather.TabIndex = 26;
            // 
            // txtPGrandFather
            // 
            this.txtPGrandFather.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPGrandFather.Location = new System.Drawing.Point(144, 62);
            this.txtPGrandFather.Name = "txtPGrandFather";
            this.txtPGrandFather.Size = new System.Drawing.Size(220, 20);
            this.txtPGrandFather.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Grand Father ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(394, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Grand Mother";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(394, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Grand Mother";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Grand Father ";
            // 
            // txtMotherName
            // 
            this.txtMotherName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMotherName.Location = new System.Drawing.Point(521, 16);
            this.txtMotherName.Name = "txtMotherName";
            this.txtMotherName.Size = new System.Drawing.Size(220, 20);
            this.txtMotherName.TabIndex = 20;
            // 
            // txtFathersName
            // 
            this.txtFathersName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFathersName.Location = new System.Drawing.Point(144, 21);
            this.txtFathersName.Name = "txtFathersName";
            this.txtFathersName.Size = new System.Drawing.Size(220, 20);
            this.txtFathersName.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(394, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Mothers Name";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 17;
            this.label17.Text = "Fathers Name";
            // 
            // tabPersonalDetails
            // 
            this.tabPersonalDetails.Controls.Add(this.tabPage7);
            this.tabPersonalDetails.Location = new System.Drawing.Point(15, 15);
            this.tabPersonalDetails.Name = "tabPersonalDetails";
            this.tabPersonalDetails.SelectedIndex = 0;
            this.tabPersonalDetails.Size = new System.Drawing.Size(399, 150);
            this.tabPersonalDetails.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage7.Controls.Add(this.rdoFemale);
            this.tabPage7.Controls.Add(this.rdoMale);
            this.tabPage7.Controls.Add(this.dtpBirthDate);
            this.tabPage7.Controls.Add(this.label15);
            this.tabPage7.Controls.Add(this.label14);
            this.tabPage7.Controls.Add(this.txtFullName);
            this.tabPage7.Controls.Add(this.label2);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(391, 124);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Personal Details";
            // 
            // rdoFemale
            // 
            this.rdoFemale.AutoSize = true;
            this.rdoFemale.Location = new System.Drawing.Point(213, 93);
            this.rdoFemale.Name = "rdoFemale";
            this.rdoFemale.Size = new System.Drawing.Size(59, 17);
            this.rdoFemale.TabIndex = 42;
            this.rdoFemale.TabStop = true;
            this.rdoFemale.Text = "Female";
            this.rdoFemale.UseVisualStyleBackColor = true;
            // 
            // rdoMale
            // 
            this.rdoMale.AutoSize = true;
            this.rdoMale.Location = new System.Drawing.Point(129, 93);
            this.rdoMale.Name = "rdoMale";
            this.rdoMale.Size = new System.Drawing.Size(48, 17);
            this.rdoMale.TabIndex = 41;
            this.rdoMale.TabStop = true;
            this.rdoMale.Text = "Male";
            this.rdoMale.UseVisualStyleBackColor = true;
            // 
            // dtpBirthDate
            // 
            this.dtpBirthDate.Location = new System.Drawing.Point(129, 57);
            this.dtpBirthDate.Name = "dtpBirthDate";
            this.dtpBirthDate.Size = new System.Drawing.Size(224, 20);
            this.dtpBirthDate.TabIndex = 40;
            this.dtpBirthDate.Value = new System.DateTime(2015, 1, 3, 0, 0, 0, 0);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 39;
            this.label15.Text = "Gender";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Date of Birth";
            // 
            // txtFullName
            // 
            this.txtFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFullName.Location = new System.Drawing.Point(129, 18);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(224, 20);
            this.txtFullName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Full Name";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvSearchResult_Birth);
            this.tabPage4.Controls.Add(this.tabControl1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(819, 584);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Search";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgvSearchResult_Birth
            // 
            this.dgvSearchResult_Birth.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearchResult_Birth.Location = new System.Drawing.Point(24, 163);
            this.dgvSearchResult_Birth.Name = "dgvSearchResult_Birth";
            this.dgvSearchResult_Birth.Size = new System.Drawing.Size(752, 368);
            this.dgvSearchResult_Birth.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Location = new System.Drawing.Point(22, 19);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(758, 100);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage10
            // 
            this.tabPage10.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage10.Controls.Add(this.dtpSearchDateOfBirth);
            this.tabPage10.Controls.Add(this.txtSearchFirstName);
            this.tabPage10.Controls.Add(this.label19);
            this.tabPage10.Controls.Add(this.label18);
            this.tabPage10.Controls.Add(this.label12);
            this.tabPage10.Controls.Add(this.label10);
            this.tabPage10.Controls.Add(this.txtSearchLastName);
            this.tabPage10.Controls.Add(this.btnSearch);
            this.tabPage10.Controls.Add(this.btnRefresh);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(750, 74);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "Criteria";
            // 
            // dtpSearchDateOfBirth
            // 
            this.dtpSearchDateOfBirth.Location = new System.Drawing.Point(353, 7);
            this.dtpSearchDateOfBirth.Name = "dtpSearchDateOfBirth";
            this.dtpSearchDateOfBirth.Size = new System.Drawing.Size(200, 20);
            this.dtpSearchDateOfBirth.TabIndex = 11;
            // 
            // txtSearchFirstName
            // 
            this.txtSearchFirstName.Location = new System.Drawing.Point(82, 6);
            this.txtSearchFirstName.Name = "txtSearchFirstName";
            this.txtSearchFirstName.Size = new System.Drawing.Size(156, 20);
            this.txtSearchFirstName.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "Last Name";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(269, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Date Of Birth";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(363, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "First Name";
            // 
            // txtSearchLastName
            // 
            this.txtSearchLastName.Location = new System.Drawing.Point(82, 37);
            this.txtSearchLastName.Name = "txtSearchLastName";
            this.txtSearchLastName.Size = new System.Drawing.Size(156, 20);
            this.txtSearchLastName.TabIndex = 2;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(559, 39);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(655, 39);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(900, 668);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Marriage";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage15);
            this.tabControl2.Location = new System.Drawing.Point(37, 29);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(827, 610);
            this.tabControl2.TabIndex = 33;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tabControl3);
            this.tabPage9.Controls.Add(this.button1);
            this.tabPage9.Controls.Add(this.button2);
            this.tabPage9.Controls.Add(this.tabControl6);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(819, 584);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Registration";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage11);
            this.tabControl3.Location = new System.Drawing.Point(21, 290);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(763, 248);
            this.tabControl3.TabIndex = 5;
            // 
            // tabPage11
            // 
            this.tabPage11.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage11.Controls.Add(this.txtWProfession);
            this.tabPage11.Controls.Add(this.label11);
            this.tabPage11.Controls.Add(this.txtWBaptizedChurch);
            this.tabPage11.Controls.Add(this.label20);
            this.tabPage11.Controls.Add(this.txtWResitingAt);
            this.tabPage11.Controls.Add(this.label27);
            this.tabPage11.Controls.Add(this.txtBirthPlace);
            this.tabPage11.Controls.Add(this.label28);
            this.tabPage11.Controls.Add(this.txtWMothersName);
            this.tabPage11.Controls.Add(this.txtWFathersName);
            this.tabPage11.Controls.Add(this.txtWLastName);
            this.tabPage11.Controls.Add(this.label29);
            this.tabPage11.Controls.Add(this.txtWFirstName);
            this.tabPage11.Controls.Add(this.label30);
            this.tabPage11.Controls.Add(this.label33);
            this.tabPage11.Controls.Add(this.label34);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(755, 222);
            this.tabPage11.TabIndex = 1;
            this.tabPage11.Text = "Wife Details";
            // 
            // txtWProfession
            // 
            this.txtWProfession.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWProfession.Location = new System.Drawing.Point(499, 161);
            this.txtWProfession.Name = "txtWProfession";
            this.txtWProfession.Size = new System.Drawing.Size(224, 20);
            this.txtWProfession.TabIndex = 45;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(389, 161);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "Profession";
            // 
            // txtWBaptizedChurch
            // 
            this.txtWBaptizedChurch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWBaptizedChurch.Location = new System.Drawing.Point(124, 154);
            this.txtWBaptizedChurch.Name = "txtWBaptizedChurch";
            this.txtWBaptizedChurch.Size = new System.Drawing.Size(224, 20);
            this.txtWBaptizedChurch.TabIndex = 43;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(21, 161);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 13);
            this.label20.TabIndex = 42;
            this.label20.Text = "Baptized Church";
            // 
            // txtWResitingAt
            // 
            this.txtWResitingAt.AcceptsReturn = true;
            this.txtWResitingAt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWResitingAt.Location = new System.Drawing.Point(499, 119);
            this.txtWResitingAt.Name = "txtWResitingAt";
            this.txtWResitingAt.Size = new System.Drawing.Size(224, 20);
            this.txtWResitingAt.TabIndex = 41;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(393, 121);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 13);
            this.label27.TabIndex = 40;
            this.label27.Text = "Residing at";
            // 
            // txtBirthPlace
            // 
            this.txtBirthPlace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBirthPlace.Location = new System.Drawing.Point(124, 114);
            this.txtBirthPlace.Name = "txtBirthPlace";
            this.txtBirthPlace.Size = new System.Drawing.Size(224, 20);
            this.txtBirthPlace.TabIndex = 39;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(21, 114);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 13);
            this.label28.TabIndex = 38;
            this.label28.Text = "Birth Place";
            // 
            // txtWMothersName
            // 
            this.txtWMothersName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWMothersName.Location = new System.Drawing.Point(499, 72);
            this.txtWMothersName.Name = "txtWMothersName";
            this.txtWMothersName.Size = new System.Drawing.Size(224, 20);
            this.txtWMothersName.TabIndex = 37;
            // 
            // txtWFathersName
            // 
            this.txtWFathersName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWFathersName.Location = new System.Drawing.Point(124, 67);
            this.txtWFathersName.Name = "txtWFathersName";
            this.txtWFathersName.Size = new System.Drawing.Size(224, 20);
            this.txtWFathersName.TabIndex = 36;
            // 
            // txtWLastName
            // 
            this.txtWLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWLastName.Location = new System.Drawing.Point(499, 21);
            this.txtWLastName.Name = "txtWLastName";
            this.txtWLastName.Size = new System.Drawing.Size(224, 20);
            this.txtWLastName.TabIndex = 19;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(393, 28);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(58, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Last Name";
            // 
            // txtWFirstName
            // 
            this.txtWFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWFirstName.Location = new System.Drawing.Point(124, 21);
            this.txtWFirstName.Name = "txtWFirstName";
            this.txtWFirstName.Size = new System.Drawing.Size(224, 20);
            this.txtWFirstName.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(21, 28);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "First Name";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(21, 74);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(73, 13);
            this.label33.TabIndex = 17;
            this.label33.Text = "Fathers Name";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(393, 74);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(76, 13);
            this.label34.TabIndex = 18;
            this.label34.Text = "Mothers Name";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(688, 555);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(591, 555);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Register";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tabControl6
            // 
            this.tabControl6.Controls.Add(this.tabPage14);
            this.tabControl6.Location = new System.Drawing.Point(17, 19);
            this.tabControl6.Name = "tabControl6";
            this.tabControl6.SelectedIndex = 0;
            this.tabControl6.Size = new System.Drawing.Size(763, 248);
            this.tabControl6.TabIndex = 0;
            // 
            // tabPage14
            // 
            this.tabPage14.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage14.Controls.Add(this.txtHProfession);
            this.tabPage14.Controls.Add(this.label24);
            this.tabPage14.Controls.Add(this.txtHBaptizedChurch);
            this.tabPage14.Controls.Add(this.label23);
            this.tabPage14.Controls.Add(this.txtHResidingAt);
            this.tabPage14.Controls.Add(this.label22);
            this.tabPage14.Controls.Add(this.txtHBirthPlace);
            this.tabPage14.Controls.Add(this.label21);
            this.tabPage14.Controls.Add(this.txtHMothersName);
            this.tabPage14.Controls.Add(this.txtHFathersName);
            this.tabPage14.Controls.Add(this.txtHLastName);
            this.tabPage14.Controls.Add(this.label35);
            this.tabPage14.Controls.Add(this.txtHFirstName);
            this.tabPage14.Controls.Add(this.label36);
            this.tabPage14.Controls.Add(this.label32);
            this.tabPage14.Controls.Add(this.label31);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(755, 222);
            this.tabPage14.TabIndex = 1;
            this.tabPage14.Text = "Husband Details";
            // 
            // txtHProfession
            // 
            this.txtHProfession.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHProfession.Location = new System.Drawing.Point(499, 161);
            this.txtHProfession.Name = "txtHProfession";
            this.txtHProfession.Size = new System.Drawing.Size(224, 20);
            this.txtHProfession.TabIndex = 45;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(389, 161);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 13);
            this.label24.TabIndex = 44;
            this.label24.Text = "Profession";
            // 
            // txtHBaptizedChurch
            // 
            this.txtHBaptizedChurch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHBaptizedChurch.Location = new System.Drawing.Point(124, 154);
            this.txtHBaptizedChurch.Name = "txtHBaptizedChurch";
            this.txtHBaptizedChurch.Size = new System.Drawing.Size(224, 20);
            this.txtHBaptizedChurch.TabIndex = 43;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(21, 161);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(85, 13);
            this.label23.TabIndex = 42;
            this.label23.Text = "Baptized Church";
            // 
            // txtHResidingAt
            // 
            this.txtHResidingAt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHResidingAt.Location = new System.Drawing.Point(499, 119);
            this.txtHResidingAt.Name = "txtHResidingAt";
            this.txtHResidingAt.Size = new System.Drawing.Size(224, 20);
            this.txtHResidingAt.TabIndex = 41;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(393, 121);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 40;
            this.label22.Text = "Residing at";
            // 
            // txtHBirthPlace
            // 
            this.txtHBirthPlace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHBirthPlace.Location = new System.Drawing.Point(124, 114);
            this.txtHBirthPlace.Name = "txtHBirthPlace";
            this.txtHBirthPlace.Size = new System.Drawing.Size(224, 20);
            this.txtHBirthPlace.TabIndex = 39;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(21, 114);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = "Birth Place";
            // 
            // txtHMothersName
            // 
            this.txtHMothersName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHMothersName.Location = new System.Drawing.Point(499, 72);
            this.txtHMothersName.Name = "txtHMothersName";
            this.txtHMothersName.Size = new System.Drawing.Size(224, 20);
            this.txtHMothersName.TabIndex = 37;
            // 
            // txtHFathersName
            // 
            this.txtHFathersName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHFathersName.Location = new System.Drawing.Point(124, 67);
            this.txtHFathersName.Name = "txtHFathersName";
            this.txtHFathersName.Size = new System.Drawing.Size(224, 20);
            this.txtHFathersName.TabIndex = 36;
            // 
            // txtHLastName
            // 
            this.txtHLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHLastName.Location = new System.Drawing.Point(499, 21);
            this.txtHLastName.Name = "txtHLastName";
            this.txtHLastName.Size = new System.Drawing.Size(224, 20);
            this.txtHLastName.TabIndex = 19;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(393, 28);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 13);
            this.label35.TabIndex = 12;
            this.label35.Text = "Last Name";
            // 
            // txtHFirstName
            // 
            this.txtHFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHFirstName.Location = new System.Drawing.Point(124, 21);
            this.txtHFirstName.Name = "txtHFirstName";
            this.txtHFirstName.Size = new System.Drawing.Size(224, 20);
            this.txtHFirstName.TabIndex = 4;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(21, 28);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(57, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "First Name";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(21, 74);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(73, 13);
            this.label32.TabIndex = 17;
            this.label32.Text = "Fathers Name";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(393, 74);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(76, 13);
            this.label31.TabIndex = 18;
            this.label31.Text = "Mothers Name";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.dataGridView1);
            this.tabPage15.Controls.Add(this.tabControl7);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(819, 584);
            this.tabPage15.TabIndex = 2;
            this.tabPage15.Text = "Search";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(24, 163);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(752, 368);
            this.dataGridView1.TabIndex = 1;
            // 
            // tabControl7
            // 
            this.tabControl7.Controls.Add(this.tabPage16);
            this.tabControl7.Location = new System.Drawing.Point(22, 19);
            this.tabControl7.Name = "tabControl7";
            this.tabControl7.SelectedIndex = 0;
            this.tabControl7.Size = new System.Drawing.Size(758, 100);
            this.tabControl7.TabIndex = 0;
            // 
            // tabPage16
            // 
            this.tabPage16.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage16.Controls.Add(this.dateTimePicker2);
            this.tabPage16.Controls.Add(this.textBox13);
            this.tabPage16.Controls.Add(this.label37);
            this.tabPage16.Controls.Add(this.label38);
            this.tabPage16.Controls.Add(this.label39);
            this.tabPage16.Controls.Add(this.label40);
            this.tabPage16.Controls.Add(this.textBox14);
            this.tabPage16.Controls.Add(this.button3);
            this.tabPage16.Controls.Add(this.button4);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(750, 74);
            this.tabPage16.TabIndex = 1;
            this.tabPage16.Text = "Criteria";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(353, 7);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 11;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(82, 6);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(156, 20);
            this.textBox13.TabIndex = 10;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 34);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 13);
            this.label37.TabIndex = 7;
            this.label37.Text = "Last Name";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(269, 13);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(68, 13);
            this.label38.TabIndex = 6;
            this.label38.Text = "Date Of Birth";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(363, 39);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(0, 13);
            this.label39.TabIndex = 5;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(7, 9);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(57, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "First Name";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(82, 37);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(156, 20);
            this.textBox14.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(559, 39);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(655, 39);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Refresh";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(900, 668);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Death ";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage12);
            this.tabControl4.Location = new System.Drawing.Point(13, 359);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(781, 88);
            this.tabControl4.TabIndex = 4;
            // 
            // tabPage12
            // 
            this.tabPage12.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage12.Controls.Add(this.label41);
            this.tabPage12.Controls.Add(this.label13);
            this.tabPage12.Controls.Add(this.txtGodMother);
            this.tabPage12.Controls.Add(this.txtGodFather);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(773, 62);
            this.tabPage12.TabIndex = 0;
            this.tabPage12.Text = "Misc";
            // 
            // txtGodFather
            // 
            this.txtGodFather.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGodFather.Location = new System.Drawing.Point(129, 18);
            this.txtGodFather.Name = "txtGodFather";
            this.txtGodFather.Size = new System.Drawing.Size(162, 20);
            this.txtGodFather.TabIndex = 33;
            // 
            // txtGodMother
            // 
            this.txtGodMother.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGodMother.Location = new System.Drawing.Point(523, 13);
            this.txtGodMother.Name = "txtGodMother";
            this.txtGodMother.Size = new System.Drawing.Size(220, 20);
            this.txtGodMother.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "God Father ";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(396, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(63, 13);
            this.label41.TabIndex = 35;
            this.label41.Text = "God Mother";
            // 
            // frmRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1123, 750);
            this.Controls.Add(this.tabRegistration);
            this.Name = "frmRegistration";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Registration";
            this.tabRegistration.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabBirth.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabRemarrks.ResumeLayout(false);
            this.tabRemarks.ResumeLayout(false);
            this.tabRemarks.PerformLayout();
            this.tabAddressDetails.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabFamilyDetails.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPersonalDetails.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearchResult_Birth)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabControl6.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl7.ResumeLayout(false);
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.tabControl4.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabRegistration;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabControl tabBirth;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabControl tabAddressDetails;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TabControl tabFamilyDetails;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox txtMotherName;
        private System.Windows.Forms.TextBox txtFathersName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabControl tabPersonalDetails;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFullName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.RadioButton rdoFemale;
        private System.Windows.Forms.RadioButton rdoMale;
        private System.Windows.Forms.DateTimePicker dtpBirthDate;
        private System.Windows.Forms.TextBox txtPGrandFather;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMGrandFather;
        private System.Windows.Forms.TextBox txtMGrandMother;
        private System.Windows.Forms.TextBox txtPGrandMother;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TabControl tabRemarrks;
        private System.Windows.Forms.TabPage tabRemarks;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TextBox txtSearchFirstName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSearchLastName;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DateTimePicker dtpSearchDateOfBirth;
        private System.Windows.Forms.DataGridView dgvSearchResult_Birth;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabControl tabControl6;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TextBox txtHLastName;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtHFirstName;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl tabControl7;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtHMothersName;
        private System.Windows.Forms.TextBox txtHFathersName;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtHBirthPlace;
        private System.Windows.Forms.TextBox txtHResidingAt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtHBaptizedChurch;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtHProfession;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TextBox txtWProfession;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtWBaptizedChurch;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtWResitingAt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtBirthPlace;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtWMothersName;
        private System.Windows.Forms.TextBox txtWFathersName;
        private System.Windows.Forms.TextBox txtWLastName;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtWFirstName;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TextBox txtGodMother;
        private System.Windows.Forms.TextBox txtGodFather;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label13;
    }
}